const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');


const mongoUrl = require('./api/config/config');
const userRoutes = require('./api/routes/user');
const productRoutes = require('./api/routes/product');
const cartRoutes = require('./api/routes/cart');
const categoryRoutes=require('./api/routes/category');
const orderRoutes=require('./api/routes/order');

mongoose.connect(mongoUrl.url.baseUrl, { useNewUrlParser: true, useUnifiedTopology: true  });

app.use(bodyParser.json({limit: '50mb'}));
app.use(cors());
app.use('/user',userRoutes);
app.use('/product',productRoutes);
app.use('/cart',cartRoutes);
app.use('/category',categoryRoutes);
app.use('/order',orderRoutes)


module.exports = app;
