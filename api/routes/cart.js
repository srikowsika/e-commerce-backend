const express = require('express');
//const mongoose = require('mongoose');
const router = express.Router();
const cart = require('../models/cart');
const product = require('../models/product');
const user = require('../models/user');
router.get('/', (req, res, next) => {
  cart.find()
    .then(docs => {
      res.status(200).json(docs);
    })
    .catch(error => {
      res.status(500).json({ error: error });
    });
})

router.get('/:userId', (req, res, next) => {
  cart.find({ userId: req.params.userId })
    .then(docs => {
      res.status(200).json(docs);
    })
    .catch(error => {
      res.status(500).json({ error: error });
    });
})

router.post('/:userId', (req, res, next) => {
  user.find({ _id: req.params.userId })
    .then(data => {
      product.find({ _id: req.body.productdetails[0].productId })
        .then(data => {
          if (data[0].productAvailability >= req.body.productdetails[0].quantity) {
            if (data.length > 0) {
              cart.find({ userId: req.params.userId })
                .then(docs => {
                  var productObject = { productId: req.body.productdetails[0].productId, quantity: req.body.productdetails[0].quantity }
                  cart.findOneAndUpdate(
                    { userId: req.params.userId },
                    {
                      $push: { productdetails: productObject }
                    })
                    .then(docs => {

                      res.status(200).json({
                        message: "Product Added to cart",
                        message: docs
                      });
                    })
                    .catch(err => console.log(err))
                })
                .catch(err => {
                  const cartDetails = new cart({
                    _id: new mongoose.Types.ObjectId(),
                    userId: req.params.userId,
                    productdetails: req.body.productdetails
                  });

                  cartDetails.save()
                    .then(docs => {
                      res.status(200).json({
                        message: "Product Added to cart",
                      });
                    })
                    .catch(err => console.log(err))
                })
            }
          }
          else {
            res.status(400).json({
              message: "Your requirement exceeds the availability",
            });
          }
        })
        .catch(err =>
          res.status(400).json({
            message: 'Enter a valid productId'
          }))
    })
    .catch(err => {
      res.status(400).json({
        message: 'Enter a valid userId'
      })
    })
})

router.delete('/:userId/:productId', (req, res, next) => {
  user.find({ _id: req.params.userId })
    .then(data => {
      product.find({ _id: req.params.productId })
        .then(data => {
          cart.find({ userId: req.params.userId }, (error, data) => {
            if (data.length > 0) {
              cart.findOneAndUpdate(
                { userId: req.params.userId },
                { $pull: { "productdetails": { productId: req.params.productId } } })
                .then(docs => {
                  res.status(200).json({
                    message: "Product removed to cart",
                  });
                })
            }
          })
        })
        .catch(err => {
          res.status(400).json({ message: 'Enter a valid productID' })
        })

    })
    .catch(err => {
      res.status(400).json({
        message: 'Enter a valid userId'
      })
    })

})
router.delete('/:_id', (req, res, next) => {
  cart.deleteOne({ _id: req.params._id })
    .then(docs => {
      res.status(200).json(docs);
    })
    .catch(error => {
      res.status(500).json({ message: 'Invalid id', });

    });
})
module.exports = router;