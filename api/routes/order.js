const express = require('express');
const router = express.Router();
const order = require('../models/order');
const product=require('../models/product');

router.get('/',(req,res,next) =>{
    order.find()
    .then(docs => {
      res.status(200).json(docs);
    })
    .catch(error => {
      res.status(500).json({ error: error });
    });
})
router.get('/:userId',(req,res,next)=>{
    order.find({userId:req.params.userId})
    .then(docs => {
        res.status(200).json(docs);
    })

})
router.post('/:userId',(req,res,next) =>{
      product.find({_id: req.body.productId})
        .then(data=>
        {
            if(data[0]. productAvailability>=req.body.quantity) {
                const availableQuantity = data[0]. productAvailability-req.body.quantity;
                product.findOneAndUpdate({_id:req.body.productId},
                  { $set:{productAvailability:availableQuantity} } )
                  .then(docs =>{
                     
                    res.status(200).json(docs);
                    const orderDetails = new order({
                        _id: new mongoose.Types.ObjectId(),
                        userId:req.params.userId,
                        productId:req.body.productId,
                        quantity:req.body.quantity,
                        price: req.body.quantity * docs.productPrice,
                        paymentMode:req.body.paymentMode,
                        address:req.body.address
                      });
                    orderDetails.save().then(data =>
                        {
                            res.status(200).json({message : "Order Placed"});
                        }
                        );
                    })
                    .catch( error=> {
                     res.status(500).json({message:error});
                    });
                  }
                  else{
                   res.status(500).json({message:"your requirement exceeds the quantity"});
                  }
        })
    })

router.delete('/:orderId', (req, res, next) => {
     order.deleteOne({ _id: req.params.orderId })
      .then(docs => {
        res.status(200).json(docs);
      })
      .catch(error => {
        res.status(500).json({ message: 'Invalid order id', });
      });

    })
module.exports=router;