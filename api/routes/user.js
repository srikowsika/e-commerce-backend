
const express = require('express');
const router = express.Router();
const user = require('../models/user');
const jwt = require('jsonwebtoken');

router.get("/:userId", (req, res, next) => {
  user.find({ _id: req.params.userId })
    .then(docs => {
      res.status(200).json(docs);
    })
    .catch(error => {
      res.status(500).json({ error: error });
    });
});

router.get("/", (req, res, next) => {
  user.find()
    .then(docs => {
      res.status(200).json(docs);
    })
    .catch(error => {
      res.status(500).json({ error: error });
    });
});
router.post('/', (req, res, next) => {

  const signupDetails = new user({
    _id: new mongoose.Types.ObjectId(),
    username: req.body.username,
    email: req.body.email,
    password: req.body.password,
    mobilenumber: req.body.mobilenumber,

  });

  user.find({ email: req.body.email })
    .then(data => {
      if (data.length) {
        res.status(500).json({
          message: "Invalid user"
        });
      }
    })
    .catch(data => {
      signupDetails.save()
        .then(res => {
          res.status(200).json({
            message: "valid Id",
            username: signupDetails.username,
          });
        })
        .catch(err => {
          res.status(400).json({ message: err.message })
        })
    })
});
router.put('/:userId', (req, res, next) => {
  const id = req.params.userId;
  user.findOneAndUpdate({ _id: id }, req.body)
    .then(result => {
      res.status(200).json(result);
    })
    .catch(error => {
      res.status(500).json({ error: error });
    });
});
router.post('/login', (req, res, next) => {
  const loginDetails = new user({
    id: new mongoose.Types.ObjectId(),
    email: req.body.email,
    password: req.body.password
  });
  user.find({ email: req.body.email, password: req.body.password },
    (error, data) => {
      if (data.length) {
        const payload = { userId: data[0].id, userName: data[0].username };
        const loginToken = jwt.sign(payload, 'login');
        res.status(200).json({
          message: "Login succesfully",
          token: loginToken,
          userId:data[0].id,
          userType: data[0].userType,
          userName:data[0].username
        });
      }
      else {
        res.status(400).json({
          message: "Incorrect username or password"
        });
      }
    })
})
router.delete('/:userId', (req, res, next) => {
  const id = req.params.userId;
  user.deleteOne({ _id: id })
    .then(result => {
      console.log(result)
      res.status(200).json(result);
    })
    .catch(error => {
      res.status(500).json({ error: error });
    });
});
module.exports = router;
