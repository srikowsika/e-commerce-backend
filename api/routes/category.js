const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const category = require('../models/category');


router.get('/',(req,res,next) =>{
    category.find()
    .then(docs => {
      res.status(200).json(docs);
    })
    .catch(error => {
      res.status(500).json({ error: error });
    });
})
router.get('/:categoryId',(req,res,next)=>{
    category.find({_id:req.params.categoryId})
    .then(docs => {
        res.status(200).json(docs);
    })
    .catch(error=>{
      res.status(404).json({
        message:error
      })
    })
})
router.post('/',(req,res,next) =>{
    const categoryDetails = new category({
        _id: new mongoose.Types.ObjectId(),
        category:req.body.category
      });
    category.find({category:req.body.category})
       .then(data =>{
        if(data.length){
          res.status(500).json({
           message:"Category Already exists"
          });
        }
        else{
            categoryDetails.save()
            res.status(200).json({
              message: "Category Added",
              category:req.body.category
          
            });
        }
      })
})
router.put('/:categoryId', (req, res, next) => {
    category.findOneAndUpdate({ _id: req.params.categoryId },req.body)
      
      .then(docs => {
        res.status(200).json(docs);
      })
      .catch(error => {
        res.status(500).json({ message: 'Invalid category id', });
  
      });
  })

router.delete('/:categoryId', (req, res, next) => {
    category.deleteOne({ _id: req.params.categoryId })
      
      .then(docs => {
        res.status(200).json(docs);
      })
      .catch(error => {
        res.status(500).json({ message: 'Invalid category id', });
      });

    })
module.exports=router;