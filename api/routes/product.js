const express = require('express');
const router = express.Router();
const product = require('../models/product');


router.get('/', (req, res, next) => {
  product.find()
    .then(docs => {

      res.status(200).json(docs);
    })
    .catch(error => {
      res.status(500).json({ error: error });
    });
})

router.get('/:productId', (req, res, next) => {
  product.find({ _id: req.params.productId })
    .then(docs => {
      res.status(200).json(docs);
    })
    .catch(error => {
      res.status(500).json({ error: error });
    });
})

router.post('/', (req, res, next) => {
  const productDetails = new product({
    _id: new mongoose.Types.ObjectId(),
    productName: req.body.productName,
    productBrand: req.body.productBrand,
    productSpecification: req.body.productSpecification,
    productPrice: req.body.productPrice,
    productImage: req.body.productImage,
    productAvailability: req.body.productAvailability
  });
  productDetails.save()
  res.status(200).json({
    message: "Product Added",
  });
})
router.put('/:productId', (req, res, next) => {
  product.findOneAndUpdate({ _id: req.params.productId },req.body)
    .then(docs => {
      res.status(200).json(docs);
    })
    .catch(error => {
      res.status(500).json({ message: 'Invalid product id', });

    });
})
router.delete('/:productId', (req, res, next) => {
  product.deleteOne({ _id: req.params.productId })
    .then(docs => {
      res.status(200).json(docs);
    })
    .catch(error => {
      res.status(500).json({ message: 'Invalid product id', });
    });
})
module.exports = router;