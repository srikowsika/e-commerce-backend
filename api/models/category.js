const mongoose = require('mongoose')

const categorySchema = mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    category: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('category', categorySchema);