const mongoose = require('mongoose');


const productSchema = mongoose.Schema({
    _id:
    {
        type:mongoose.Schema.Types.ObjectId,
        required:true
    },
     productName:{
         type:String,
         required:true
        },
     categoryId:{
         type:mongoose.Schema.Types.ObjectId,
         required:true
        },
     productBrand:{
         type:String,
         required:true
        },
     productSpecification:{
         type:String,
         required:true
        },

     productPrice :{
        type: Number,
        required:true
     },
     productImage :{
        type:String,
        required:true
     } ,
     productAvailability : {
         type:Number,
         required:true,
        min:1}
  });
  
  module.exports = mongoose.model('product',productSchema);