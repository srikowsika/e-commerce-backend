const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    _id: {
      type: mongoose.Schema.Types.ObjectId,
      required: true
    },
    username: {
      type: String,
      required: true
    },

    email: {
      type: String,
      required: true
    },
    password: {
      type: String,
      required: true,
      minlength: 10,
      maxlength: 16
    },
    mobilenumber:
    {
      type: String,
      required: true,
      maxlength: 10
    },
    userType:
    {
      type: Number,
      default: 0
    }
});

module.exports = mongoose.model('user', userSchema);