const mongoose = require('mongoose');


const orderSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    productId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    quantity: {
        type: Number,
        min: 1,
        required: true
    },
    price: {
        type: Number,
        min: 1,
        required: true
    },
    paymentMode: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    }

});

module.exports = mongoose.model('order', orderSchema);